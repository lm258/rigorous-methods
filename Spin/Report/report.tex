%
% Author - Lewis Maitland
% Date   - 13/10/2014
% Brief  - This is the main page for my
%          dissertation report. Includes
%          the other pages for tidyness.

\documentclass[a4paper,11pt]{report}

\usepackage[dvipsnames,usenames]{color,xcolor,colortbl}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{array}
\usepackage{listings}
\usepackage{color}
\usepackage{lewis}

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstdefinestyle{prom}{
  basicstyle=\tiny,        % the size of the fonts that are used for the code
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  extendedchars=true,
  keepspaces=true,
  keywordstyle=\color{blue},       % keyword style
  language=Promela,                 % the language of the code
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,                       % sets default tab size to 2 spaces
}

\lstdefinestyle{file}{
  basicstyle=\tiny,        % the size of the fonts that are used for the code
  captionpos=b,                    % sets the caption-position to bottom
  extendedchars=true,
  keepspaces=true,
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  tabsize=2,                       % sets default tab size to 2 spaces
}

\begin{document}

%Make the title page of our document
\title{ \textbf{SPIN Trainware Report} }
\author{Lewis Maitland\\ \small Heriot-Watt University\\ \small lm258@hw.ac.uk\\ \tiny Rev: \input{revision.hash} }\maketitle

%Start our assumptions
\section*{D1: Assumptions}
The TrainWare had several assumptions. Each assumption has been listed below:
\begin{description}
    \item[Accurate Control Signaling] The implementation assumes that control signals are always adhered to. In older systems such as signalmen and telegraph communication between them, trains would be more likely to mistake control signals and continue into tunnels. Control signals are implemented using two rendezvous channels for each station. One channel is used for detecting a train entering the station, the other is used to control the train leaving.
    \item[Accurate Train Acknowledgment] Each signal box always accurately knows when trains are entering or leaving tunnels. In older real world systems a signalman could fall asleep and miss the incoming train. Signal boxes check adjacent signal boxes using two rendezvous channels, one for previous signal and one for the next.
    \item[Only two trains] Only two trains are assumed to be on the track after initialization. This is unrealistic although the implementation would allow more tracks to be added.
\end{description}

%Diagrammatic Representation
\section*{D2: Diagrammatic Representation}
\begin{figure}[ht]
\caption{\label{use-case}Diagram of TrainWare system}
\center \includegraphics[width=0.8\textwidth]{img/diagram.eps}
\end{figure}

%Promela/SPIN code
\newpage
\section*{D3: Promela/SPIN Implementation}
\lstinputlisting[style=prom]{../Assignment/trainware.pml}

%Add information about the verification effort
\newpage
\section*{D4: Verification Conditions}
\subsection*{System Monitor}
Verification of the system was first carried out using SPIN assertions.
\begin{lstlisting}[style=prom]
#define p (len(TunnelAB) < 2) && (len(TunnelBC) < 2) && (len(TunnelCD) < 2) && (len(TunnelDA) < 2)

proctype Monitor() {
    assert(p);  
}
\end{lstlisting}
Essentially the Monitor() process was used to check that the system never broke the key property of two trains
being a tunnel at the same time. If this property was broken an assertion would break the current flow of the program.\\
\begin{lstlisting}[style=file]
(Spin Version 6.3.2 -- 17 May 2014)
  + Partial Order Reduction

Full statespace search for:
  never claim           + (si)
  assertion violations  + (if within scope of claim)
  cycle checks        - (disabled by -DSAFETY)
  invalid end states  - (disabled by never claim)

State-vector 232 byte, depth reached 401, errors: 0
     1017 states, stored
     1327 states, matched
     2344 transitions (= stored+matched)
       10 atomic steps
hash conflicts:         0 (resolved)

Stats on memory usage (in Megabytes):
    0.252 equivalent memory usage for states (stored*(State-vector + overhead))
    0.385 actual memory usage for states
  128.000 memory used for hash table (-w24)
    0.534 memory used for DFS stack (-m10000)
  128.827 total actual memory usage



pan: elapsed time 0 seconds
No errors found -- did you verify all claims?
\end{lstlisting}


\subsection*{Temporal Properties}
A temporal property was also used during the verification of the solution.\\
Temporal properties allow the specification of predicate logic to proof specific properties about the solution.\\

\begin{lstlisting}[style=prom]
#define p (len(TunnelAB) < 2) && (len(TunnelBC) < 2) && (len(TunnelCD) < 2) && (len(TunnelDA) < 2)

ltl si{ [] p }
\end{lstlisting}
This property specifies that there must be less than 2 trains in a tunnel at any given time.

\begin{lstlisting}[style=file]
(Spin Version 6.3.2 -- 17 May 2014)
  + Partial Order Reduction

Full statespace search for:
  never claim           + (si)
  assertion violations  - (disabled by -A flag)
  cycle checks        - (disabled by -DSAFETY)
  invalid end states  - (disabled by never claim)

State-vector 240 byte, depth reached 316, errors: 0
      715 states, stored
      870 states, matched
     1585 transitions (= stored+matched)
       12 atomic steps
hash conflicts:         0 (resolved)

Stats on memory usage (in Megabytes):
    0.183 equivalent memory usage for states (stored*(State-vector + overhead))
    0.287 actual memory usage for states
  128.000 memory used for hash table (-w24)
    0.534 memory used for DFS stack (-m10000)
  128.730 total actual memory usage



pan: elapsed time 0 seconds
No errors found -- did you verify all claims?
\end{lstlisting}

\end{document}