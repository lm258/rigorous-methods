/***********************************************************************************/
/*                                                                                 */
/*                          TrainWare Control System                               */
/*                                                                                 */
/*             Andrew Ireland 29/10/03  - Adapted by Lewis Maitland                */
/*                                                                                 */
/***********************************************************************************/

/*Create our tunnel channels*/
chan TunnelAB = [2] of { bit };
chan TunnelBC = [2] of { bit };
chan TunnelCD = [2] of { bit };
chan TunnelDA = [2] of { bit };

/*Signals*/
chan SignalAB = [0] of { byte };
chan SignalBC = [0] of { byte };
chan SignalCD = [0] of { byte };
chan SignalDA = [0] of { byte };

/*Create our control signal for stations*/
chan ControlA = [0] of { bit };
chan ControlB = [0] of { bit };
chan ControlC = [0] of { bit };
chan ControlD = [0] of { bit };

chan ControlStart = [2] of { bit };

#define go 1   /*Go control signal*/
#define stop 0 /*Stop control signal*/
#define free 2 /*Used to check for trains*/


/*Create our system invariant*/
#define p (len(TunnelAB) < 2) && (len(TunnelBC) < 2) && (len(TunnelCD) < 2) && (len(TunnelDA) < 2)

/*System Invariant P should always be true*/
ltl si{ [] p }

/*Simple model of a station*/
proctype Station(chan in_track, out_track, control ) {   
    bit train;

    progress:
    do
    ::  in_track?train; control!stop; control?go; out_track!train;
    od
}

/*Signal box controls a station*/
proctype SignalBox( chan from_s, to_s, control ) {

    progress:
    do
    ::  control!go;   to_s?free;
    ::  control?stop; from_s!free;
    od
}

/*Used to check for our system invariant using assertions*/
proctype Monitor() {
    assert(p);
}

/*Put trains on the track*/
proctype Setup( chan track, control; bit train ){
    control?go;
    track!train;
}

init {
        atomic {

            /*run our monitor*/
            run Monitor();

            /*Add trains to tracks*/ 
            run Setup(TunnelBC, ControlB, 0);
            run Setup(TunnelDA, ControlD, 1);

            /*Signal Boxes, each knows previous and next signal*/
            run Station(   TunnelDA, TunnelAB, ControlA );
            run SignalBox( SignalDA, SignalAB, ControlA );
            
            run Station(   TunnelBC, TunnelCD, ControlC );
            run SignalBox( SignalBC, SignalCD, ControlC );

            run Station(   TunnelCD, TunnelDA, ControlD );
            run SignalBox( SignalCD, SignalDA, ControlD );
            
            run Station(   TunnelAB, TunnelBC, ControlB );
            run SignalBox( SignalAB, SignalBC, ControlB );
        }
}
