
#define p 0
#define v 1

chan sema = [0] of { bit };
proctype semaphore()
{	do
	:: sema!p -> sema?v
	od
}
byte count;
proctype user()
{
    do
    :: sema?p;
    ::     count = count + 1;
    ::     skip;
    ::     count = count - 1;
	:: sema!v;
    ::    skip
    od
}
proctype monitor()
{
    assert(count == 0 || count == 1) 
}

init
{	atomic {
		run semaphore();
		run monitor();
       	run user();         	
        run user();         	
        run user()
    }
}
