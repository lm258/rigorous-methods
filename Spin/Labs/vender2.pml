mtype = { milk, plain };

chan coin_channel = [1] of { byte };
chan choc_channel = [1] of { mtype };

proctype customer(){
         do
         :: coin_channel!20;
            choc_channel?milk;
         :: coin_channel!50;
            choc_channel?plain;
         od
}

proctype vender(){
         byte milk_bars = 10, plain_bars = 5;
         int  coin_box = 0;

         do
         :: ((milk_bars > 0) && coin_channel?[20]) -> 
                               coin_channel?20;
                               choc_channel!milk;
                               milk_bars = milk_bars-1;
                               coin_box = coin_box + 20;
         :: ((plain_bars > 0) && coin_channel?[50]) -> 
                                coin_channel?50;
                                choc_channel!plain;
                                plain_bars = plain_bars-1;
                                coin_box = coin_box + 50;
         od
}

init { atomic { run vender(); run customer(); }}

