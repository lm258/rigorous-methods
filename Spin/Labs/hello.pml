
proctype hello(){ printf("Hello\n") }

proctype world(){ printf("World\n") }

init { atomic { run hello(); run world () } }

