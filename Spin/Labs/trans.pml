
/* Simple 3 process transactional model augmented with the "Devil" process
   which non-deterministically disengages process B. 
 */

mtype = {engage, disengage, ack_engage, ack_disengage, dec, ack_dec}

chan A2B = [0] of { mtype }
chan B2C = [0] of { mtype }
chan C2B = [0] of { mtype }
chan B2A = [0] of { mtype }

byte B_bal = 10; 
byte C_bal = 10

bool B_engaged = false;

proctype A(){ 

  do
   :: A2B!engage; assert(B_bal == C_bal); B2A?ack_engage; A2B!dec; 
                           {B2A?ack_dec; A2B!disengage;} unless timeout; 
  od
}

proctype Devil(){

do   
   :: skip;
   :: B_engaged = false; B_engaged == true;
od
}

proctype B(){

   do
   :: B_engaged == false -> A2B?engage; B_engaged = true; B2A!ack_engage; 
   :: B_engaged == true -> 
      if
      :: A2B?dec -> B2C!dec;
      :: C2B?ack_dec -> {B_bal = B_bal -1; B2A!ack_dec;} unless {B_engaged == false}; 
      :: A2B?disengage; B_engaged = false;
      fi; 
   od
}

proctype C(){ 

   do
   :: B2C?dec -> C_bal = C_bal - 1; C2B!ack_dec;
   od
} 

init { atomic{ run A(); run B(); run C();  run Devil(); }}  
