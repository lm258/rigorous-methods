
mtype = { done };

chan hello_to_control = [0] of { mtype };
chan control_to_world = [0] of { mtype };

proctype hello(chan x){ printf("Hello\n"); x!done; }

proctype world(chan x){ x?done; printf("World\n"); }

proctype control(chan x, y){ x?done; y!done; }

init { atomic { run world(control_to_world); 
                run hello(hello_to_control); 
                run control(hello_to_control, control_to_world) } }

