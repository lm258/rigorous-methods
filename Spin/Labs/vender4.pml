
mtype = { milk, plain };

chan coin_channel = [1] of { byte };
chan choc_channel = [1] of { mtype };

int budget = 450, coin_box = 0;

proctype customer(){ 
	 do
         :: coin_channel!20;
            choc_channel?milk;
         :: coin_channel!50;
            choc_channel?plain;
         od
}

proctype vender(){ 
	 byte milk_bars = 10, plain_bars = 5; 

	 assert(budget+coin_box == 450);
	 do
	 :: ((milk_bars > 0) && coin_channel?[20]) -> coin_channel?20;
                               coin_box = coin_box + 20;
                               budget=budget-20;
                               choc_channel!milk;
                               milk_bars = milk_bars-1;
			       assert(budget+coin_box == 450);
         :: ((plain_bars > 0) && coin_channel?[50]) -> coin_channel?50;
                                coin_box = coin_box + 50;
				budget=budget-50;
                                choc_channel!plain;
                                plain_bars = plain_bars-1;
	                        assert(budget+coin_box == 450);
         od
}

init { atomic { run vender(); run customer(); }}



