byte value1 = 1, value2 = 2, value3 = 3;

proctype A() { value3 = value3 + value2;
               assert( value3 == 5 )
}
proctype B() { value2 = value2 + value1;
               assert( value3 == 5 )

}               
init { atomic{ run A(); run B()}}
