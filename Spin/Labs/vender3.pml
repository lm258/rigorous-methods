mtype = { milk, plain };

chan coin_channel = [1] of { byte };
chan choc_channel = [1] of { mtype };

byte milk_bars = 10, plain_bars = 5;
int  coin_box = 0;

proctype customer(){
         do
         :: coin_channel!20;
            choc_channel?milk;
         :: coin_channel!50;
            choc_channel?plain;
         od
}

proctype vender(){

         assert(coin_box == (450-(milk_bars*20 + plain_bars*50)));
         do
         :: ((milk_bars > 0) && coin_channel?[20]) ->
                               coin_channel?20;
                               choc_channel!milk;
                               milk_bars = milk_bars-1;
                               coin_box = coin_box + 20;
                               assert(coin_box == (450-(milk_bars*20 + plain_bars*50)));
         :: ((plain_bars > 0) && coin_channel?[50]) ->
                                coin_channel?50;
                                choc_channel!plain;
                                plain_bars = plain_bars-1;
                                coin_box = coin_box + 50;
                                assert(coin_box == (450-(milk_bars*20 + plain_bars*50)));
         od
}

init { atomic { run vender(); run customer(); }}
