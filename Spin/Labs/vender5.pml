
mtype = { tea, coffee, money };

chan payment = [0] of { mtype };
chan product = [0] of { mtype };

bool drink_tea, drink_coffee;
bool button_A = false;
bool button_B = false;

proctype customer(){ 

	 do
	 :: drink_tea = false; drink_coffee = false; payment!money; 
            if
            :: button_A = true -> product?tea; drink_tea = true; 
            :: button_B = true -> product?coffee; drink_coffee = true;
            fi;
         od
}

proctype vender(){ 

	 do
	 :: payment?money;  
	    if
            :: (button_A) -> button_A = false; product!tea;
	      :: (button_B) -> button_B = false; product!coffee;
            fi
         od
}

proctype monitor(){ assert(!(button_A && button_B));}

init { atomic { run vender(); run customer(); run monitor(); }}






