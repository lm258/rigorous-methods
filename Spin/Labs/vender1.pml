mtype = { coin20, coin50, milk, plain };

chan coin_channel = [0] of { mtype };
chan choc_channel = [0] of { mtype };

proctype customer(){ 
         do
         :: coin_channel!coin20;
            choc_channel?milk;
         :: coin_channel!coin50;
            choc_channel?plain;
         od
}

proctype vender(){ 
         do
         :: coin_channel?coin20;
            choc_channel!milk;
         :: coin_channel?coin50;
            choc_channel!plain;
         od
}

init { atomic { run vender(); run customer(); }}
