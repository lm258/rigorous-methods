
-- Author:              A. Ireland
--
-- Address:             School Mathematical & Computer Sciences
--                      Heriot-Watt University
--                      Edinburgh, EH14 4AS
--
-- E-mail:              a.ireland@hw.ac.uk
--
-- Last modified:       18/10/2002
--
-- Filename:            brakes.adb
--
-- Description:         Models the water boiler subsystem associated
--                      with the thermostat controller.

package body Boiler
is

   State: Boolean;

   procedure Switch_On is
   begin
      State:=True;
   end Switch_On;

   procedure Switch_Off is
   begin
      State:=False;
   end Switch_Off;

   function Switched_On return Boolean is
   begin
      return State;
   end Switched_On;

begin -- initialization
   State:= False;
end Boiler;



