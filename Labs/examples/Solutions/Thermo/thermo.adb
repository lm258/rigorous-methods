
-- Author:              A. Ireland
--
-- Address:             School Mathematical & Computer Sciences
--                      Heriot-Watt University
--                      Edinburgh, EH14 4AS
--
-- E-mail:              a.ireland@hw.ac.uk
--
-- Last modified:       19/10/2002
--
-- Filename:            thermo.adb
--
-- Description:         Implements the controller associated with the thermostat.


with Sensor, Boiler;
package body Thermo is

   procedure Control
   is
      Sensor_Value: Sensor.Sensor_Type;
   begin
      Sensor_Value:= Sensor.Read_Sensor;
      if Sensor_Value <= Min then
         Boiler.Switch_On;
      elsif Sensor_Value >= Max then
         Boiler.Switch_Off;
      end if;
   end Control;

end Thermo;



