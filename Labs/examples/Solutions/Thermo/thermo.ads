
-- Author:              A. Ireland
--
-- Address:             School Mathematical & Computer Sciences
--                      Heriot-Watt University
--                      Edinburgh, EH14 4AS
--
-- E-mail:              a.ireland@hw.ac.uk
--
-- Last modified:       19/10/2002
--
-- Filename:            thermo.ads
--
-- Description:         Implements the controller associated with the thermostat.

--# inherit Sensor, Boiler;
package Thermo
is

   Min: constant:= 25;
   Max: constant:= 30;

   procedure Control;
   --# global  in     Sensor.State;
   --#         in out Boiler.State;
   --# derives Boiler.State from Sensor.State, Boiler.State;

end Thermo;



