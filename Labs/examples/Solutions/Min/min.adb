
package body Min
is
   function Int_Min(X, Y: in Integer) return Integer
   is
     Z: Integer;
   begin
      if X > Y then
         Z:= Y;
      else
         Z:= X;
      end if;
      return Z;
   end Int_Min;
end Min;

