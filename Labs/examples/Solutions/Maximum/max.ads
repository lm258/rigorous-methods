
package Max
is
   function Int_Max(X, Y: in Integer) return Integer;
     --# return Z   => (X > Y -> Z = X) and
     --#               (Y > X -> Z = Y);

end Max;

