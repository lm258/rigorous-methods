
package body Int_Stack is

   function Empty(S: Stack) return Boolean is
   begin
      return S.Pointer = 0;
   end Empty;

   function Full(S: Stack) return Boolean is
   begin
      return S.Pointer = Stack_Size;
   end Full;

   procedure Init(S: out Stack) is
   begin
      S.Pointer:=0;
   end Init;

   procedure Push(X: in Integer; S: in out Stack) is
   begin
      S.Pointer:=S.Pointer+1;
      S.Data(S.Pointer):=X;
   end Push;

   procedure Pop(X: out Integer; S: in out Stack) is
   begin
      X:=S.Data(S.Pointer);
      S.Pointer:=S.Pointer-1;
   end Pop;

end Int_Stack;



