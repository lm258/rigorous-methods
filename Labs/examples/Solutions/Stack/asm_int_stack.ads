
package ASM_Int_Stack
   --# own Data, Pointer;
   --# initializes Data, Pointer;
is
   function Empty return Boolean;
   --# global in Pointer;

   function Full return Boolean;
   --# global in Pointer;

   procedure Push(X: in Integer);
   --# global in out Data, Pointer;
   --# derives Data from Data, Pointer, X &
   --#         Pointer from Pointer;

   procedure Pop(X: out Integer);
   --# global in     Data;
   --#        in out Pointer;
   --# derives X from Data, Pointer &
   --#         Pointer from Pointer;

end ASM_Int_Stack;

