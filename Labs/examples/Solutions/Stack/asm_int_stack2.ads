
package ASM_Int_Stack2
   --# own State;
   --# initializes State;
is
   function Empty return Boolean;
   --# global in State;

   function Full return Boolean;
   --# global in State;

   procedure Push(X: in Integer);
   --# global in out State;
   --# derives State from X, State;

   procedure Pop(X: out Integer);
   --# global in out State;
   --# derives X, State from State;

end ASM_Int_Stack2;

