
package Int_Stack is

   type Stack is private;

   function Empty(S: Stack) return Boolean;
   function Full(S: Stack) return Boolean;

   procedure Init(S: out Stack);
   --# derives S from ;

   procedure Push(X: in Integer; S: in out Stack);
   --# derives S from X, S;

   procedure Pop(X: out Integer; S: in out Stack);
   --# derives X, S from S;

private
   Stack_Size: constant:=4;
   type Pointer_Range is range 0..Stack_Size;
   subtype Index_Range is Pointer_Range range 1..Stack_Size;
   type Vector is array(Index_Range) of Integer;
   type Stack is
      record
         Data: Vector;
         Pointer: Pointer_Range;
      end record;
end Int_Stack;



