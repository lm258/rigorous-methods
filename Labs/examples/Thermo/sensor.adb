
-- Author:              A. Ireland
--
-- Address:             School Mathematical & Computer Sciences
--                      Heriot-Watt University
--                      Edinburgh, EH14 4AS
--
-- E-mail:              a.ireland@hw.ac.uk
--
-- Last modified:       13/10/2002
--
-- Filename:            sensor.adb
--
-- Description:         Model the sensor associated with the thermostat.

package body Sensor

is
   State: Sensor_Type;

   procedure Write_Sensor(Value: in Sensor_Type)
   is
   begin
      State:= Value;
   end Write_Sensor;

   function Read_Sensor return Sensor_Type
   is
   begin
      return State;
   end Read_Sensor;

begin -- initialization
   State:=0;
end Sensor;



