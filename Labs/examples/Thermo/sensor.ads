
-- Author:              A. Ireland
--
-- Address:             School Mathematical & Computer Sciences
--                      Heriot-Watt University
--                      Edinburgh, EH14 4AS
--
-- E-mail:              a.ireland@hw.ac.uk
--
-- Last modified:       18/10/2002
--
-- Filename:            sensor.ads
--
-- Description:         Models a sensor associated with the thermostat.

package Sensor
   --# own State;
   --# initializes State;
is
   subtype Sensor_Type is Integer range -40..40;

   procedure Write_Sensor(Value: in Sensor_Type);
   --# global out State;
   --# derives State from Value;

   function Read_Sensor return Sensor_Type;
   --# global in State;

end Sensor;



