
-- Author:              A. Ireland
--
-- Address:             School Mathematical & Computer Sciences
--                      Heriot-Watt University
--                      Edinburgh, EH14 4AS
--
-- E-mail:              a.ireland@hw.ac.uk
--
-- Last modified:       18/10/2002
--
-- Filename:            boiler.ads
--
-- Description:         Models the water boiler subsystem associated
--                      with the thermostat controller.

package Boiler
   --# own State;
   --# initializes State;
is
   procedure Switch_On;
   --# global out State;
   --# derives State from ;

   procedure Switch_Off;
   --# global out State;
   --# derives State from ;

   function Switched_On return Boolean;
   --# global in State;

end Boiler;



