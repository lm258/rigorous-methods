
package Inc_Value
is
   type T is range -128 .. 128;

   procedure Inc(X: in out T);

end Inc_Value;

