
with Text_IO, Inc_Value;
use  Text_IO, Inc_Value;
procedure Use_Inc is

  package Integer_INOUT is new Text_IO.Integer_IO(Integer);

  X_Value: Integer;

begin
   loop
      Text_IO.New_Line;
      Text_IO.Put("X = ");
      Integer_INOUT.Get(X_Value);
      Text_IO.Put("Increment of X is ");
      Inc_Value.Inc(T(X_Value));
      Integer_INOUT.Put(Integer(X_Value), 3);
      Text_IO.New_Line;
   end loop;

end Use_Inc;
