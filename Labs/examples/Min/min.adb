package body Min
is
    function Int_Min(X, Y: in Integer) return Integer
    is
        T: Integer;
    begin
        if X > Y then
            T := Y;
        else
            T := X;
        end if;
        return T;
    end Int_Min;

end Min;