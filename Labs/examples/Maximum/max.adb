
package body Max
is
   function Int_Max(X, Y: in Integer) return Integer

   is
     Z: Integer;
   begin
      if X > Y then
         Z:= X;
      else
         Z:= Y;
      end if;
      return Z;
   end Int_Max;
end Max;

