
-- Author:              L. Maitland
--
-- Address:             School Mathematical & Computer Sciences
--                      Heriot-Watt University
--                      Edinburgh, EH14 4AS
--
-- E-mail:              lm258@hw.ac.uk
--
-- Filename:            atp.ads
--
-- Description:         Models the ATP controller.
with Sensors, Alarm, Console, Speedo, Brakes;
use type Sensors.Sensor_Type;

--# inherit Sensors, Alarm, Console, Speedo, Brakes;
package ATP
   --# own Speed_Last;
   --# initializes Speed_Last;
is
    --The last recorded speed of the train
    Speed_Last: Speedo.Speed_Type;

    procedure Control;
    --# global in Sensors.State, Speedo.Speed, Console.Reset_Status;
    --#        in out Brakes.State, Alarm.State, Console.SPAD_Cnt, Speed_Last;
    --# derives Speed_Last from Speedo.Speed &
    --#         Brakes.State from Alarm.State, Speedo.Speed, Brakes.State, Sensors.State, Console.Reset_Status, Speed_Last &
    --#         Alarm.State from Brakes.State,Console.Reset_Status,Sensors.State,Alarm.State &
    --#         Console.SPAD_Cnt from Sensors.State,Brakes.State,Console.SPAD_Cnt;
    --# pre Console.SPAD_Cnt < Integer'Last;
    --# post (Speed_Last = Speedo.Speed);
end ATP;