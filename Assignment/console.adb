
-- Author:              L. Maitland
--
-- Address:             School Mathematical & Computer Sciences
--                      Heriot-Watt University
--                      Edinburgh, EH14 4AS
--
-- E-mail:              lm258@hw.ac.uk
--
-- Filename:            alarm.adb
--
-- Description:         Models the console associated with the ATP system, i.e. 
--                      the reset mechanism that is required to disable the 
--

package body Console
is
    -- Enable the reset status
    procedure Enable_Reset
    is
    begin
        Reset_Status := True;
    end Enable_Reset;

    -- Disable the reset status
    procedure Disable_Reset
    is
    begin
        Reset_Status := False;
    end Disable_Reset;

    -- This function is used to check the reset status
    function Reset_Enabled return Boolean
    is
    begin
        return Reset_Status;
    end Reset_Enabled;

    -- This procedure is used to increment our spad counter by one or zero if an integer overflow would occur
    procedure Inc_SPAD_Cnt
    is
    begin
        if SPAD_Cnt < Integer' Last then
            SPAD_Cnt := SPAD_Cnt + 1;
        else 
            SPAD_Cnt := 0;
        end if;
    end Inc_SPAD_Cnt;

    -- Reset our value of span counter
    procedure Reset_SPAD_Cnt
    is
    begin
        SPAD_Cnt := 0;
    end Reset_SPAD_Cnt;

    -- Return the spad counter value
    function SPAD_Cnt_Value return Integer
    is
    begin
        return SPAD_Cnt;
    end SPAD_Cnt_Value;

    -- Initalize our reset status and counter
    begin
        Reset_Status := False;
        SPAD_Cnt := 0;

end Console;