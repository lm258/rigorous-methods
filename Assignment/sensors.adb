
-- Author:              L. Maitland
--
-- Address:             School Mathematical & Computer Sciences
--                      Heriot-Watt University
--                      Edinburgh, EH14 4AS
--
-- E-mail:              lm258@hw.ac.uk
--
-- Filename:            sensors.ads
--
-- Description:         Models the 3 sensors associated with the ATP system. Note that
--                      a single sensor reading is calculated using a majority vote
--

package body Sensors
is
    -- Set the value of each of our sensors
    procedure Write_Sensors(Value_1, Value_2, Value_3: in Sensor_Type)
    is
    begin
        State := Sensor_List_Type'( Value_1, Value_2, Value_3 );
    end Write_Sensors;

    -- Read a specific index of the state
    function Read_Sensor(Sensor_Index: in Sensor_Index_Type) return Sensor_Type
    is
        Temp: Sensor_Type;
    begin
        if Sensor_Index > Sensor_Index_Type'First and Sensor_Index < Sensor_Index_Type'Last then
            Temp:= State(Sensor_Index);
        else
            Temp:= State(1);
        end if;
        return Temp;
    end Read_Sensor;

    -- This function is used to read the majority
    function Read_Sensor_Majority return Sensor_Type
    is
        Temp: Sensor_Type;
        Return_Value: Sensor_Type;
    begin
        -- Set inital return type
        Return_Value := Undef;
        Temp := State(1);

        -- Check for our state
        if Temp = State(2) or Temp = State(3) then
            Return_Value := Temp;
        elsif State(2) = State(3) then
            Return_Value := State(2);
        end if;

        return Return_Value;
    end Read_Sensor_Majority;

    -- Initalize our state variable
    begin
        State := Sensor_List_Type'(Sensor_Index_Type => Proceed);

end Sensors;