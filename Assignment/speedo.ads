
-- Author:              A. Ireland
--
-- Address:             School Mathematical & Computer Sciences
--                      Heriot-Watt University
--                      Edinburgh, EH14 4AS
--
-- E-mail:              a.ireland@hw.ac.uk
--
-- Last modified:       25/9/2013 
--
-- Filename:            speedo.ads
--
-- Description:         Models the speedo device associated with the ATP system.

package Speedo
   --# own Speed;
   --# initializes Speed;
is

   --Delcare out speed type and speed
   subtype Speed_Type is Integer range 0..150;
   Speed: Speed_Type;

   procedure Write_Speed(S: in Speed_Type);
   --# global out Speed;
   --# derives Speed from S;
   --# pre (S < Speed_Type'Last);
   --# post (Speed = S);

   function Read_Speed return Speed_Type;
   --# global in Speed;
   --# return Speed;

end Speedo;



