
-- Author:              L. Maitland
--
-- Address:             School Mathematical & Computer Sciences
--                      Heriot-Watt University
--                      Edinburgh, EH14 4AS
--
-- E-mail:              lm258@hw.ac.uk
--
-- Filename:            atp.adb
--
-- Description:         Models the ATP controller.
package body ATP
is
    -- This procedure is used to control our ATP
    procedure Control
    is
        Sensor_Value: Sensors.Sensor_Type;
    begin
        -- First Read our sensor majority value
        Sensor_Value := Sensors.Read_Sensor_Majority;

        -- If the brakes are enabled and the console reset is not enabled
        if Brakes.Activated = False then

            if Sensor_Value = Sensors.Proceed then

                -- If the alarm was enabled then disable it
                if Alarm.Enabled = True then
                    Alarm.Disable;
                end if;

            elsif Sensor_Value = Sensors.Caution then
                -- If the alarm is enabled we need to check speed is reducing
                if Alarm.Enabled = True then
                    -- Make sure we are slowing down if not enable breaks
                    if Speedo.Read_Speed >= Speed_Last then
                        Brakes.Activate;
                    end if;
                else
                    -- Otherwise we need to enable the alarm
                    Alarm.Enable;
                end if;

            elsif Sensor_Value = Sensors.Danger then
                -- If the sensors returned danger then activate the brakes and enable alarm
                Brakes.Activate;
                Alarm.Enable;
                Console.Inc_SPAD_Cnt;

            end if;

        -- Reset the brakes etc if the console has been reset
        elsif Brakes.Activated = True and Console.Reset_Enabled = True then
            -- Disable our alarm,brakes and also the console reset
            Alarm.Disable;
            Brakes.Deactivate;
        end if;

        -- Update our last Speed
        Speed_Last := Speedo.Read_Speed;

    end Control;

    -- Initalize Variables such as our last speed
    begin
        Speed_Last := 0;

end ATP;