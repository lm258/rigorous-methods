
-- Author:              L. Maitland
--
-- Address:             School Mathematical & Computer Sciences
--                      Heriot-Watt University
--                      Edinburgh, EH14 4AS
--
-- E-mail:              lm258@hw.ac.uk
--
-- Filename:            speedo.adb
--
-- Description:         Models the speedo device associated with the ATP system.
--
package body Speedo
is
    -- This procedure writes the speed or Speed_Type last incase of overflow
    procedure Write_Speed(S: in Speed_Type)
    is
    begin
        if S < Speed_Type'Last then
            Speed := S;
        else
            Speed := Speed_Type'Last;
        end if;
    end Write_Speed;

    -- This procedure gets the speed
    function Read_Speed return Speed_Type
    is
    begin
        return Speed;
    end Read_Speed;

    -- Initalize our speed
    begin
        Speed := 0;

end Speedo;