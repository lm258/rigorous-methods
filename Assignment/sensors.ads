
-- Author:              A. Ireland
--
-- Address:             School Mathematical & Computer Sciences
--                      Heriot-Watt University
--                      Edinburgh, EH14 4AS
--
-- E-mail:              a.ireland@hw.ac.uk
--
-- Last modified:       25/9/2013
--
-- Filename:            sensors.ads
--
-- Description:         Models the 3 sensors associated with the ATP system. Note that
--                      a single sensor reading is calculated using a majority vote
--                      algorithm.

package Sensors
    --# own State;
    --# initializes State;
is
    -- Declare the Types we will use for our sensor array etc
    type Sensor_Type is (Proceed, Caution, Danger, Undef);
    subtype Sensor_Index_Type is Integer range 1..3;
    type Sensor_List_Type is array (Sensor_Index_Type) of Sensor_Type;

    -- Declare our sensor state
    State: Sensor_List_Type;

    procedure Write_Sensors(Value_1, Value_2, Value_3: in Sensor_Type);
    --# global out State;
    --# derives State from Value_1, Value_2, Value_3;
    --# pre true;
    --# post State(1) = Value_1 and State(2) = Value_2 and State(3) = Value_3;

    function Read_Sensor(Sensor_Index: in Sensor_Index_Type) return Sensor_Type;
    --# global in State;
    --# pre Sensor_Index < Sensor_Index_Type'Last and Sensor_Index > 0;
    --# return State(Sensor_Index);

    function Read_Sensor_Majority return Sensor_Type;
    --# global in State;
    --# pre true;
    --# return Ans => ( State(1) = State(3) -> Ans = State(1) ) or 
    --#               ( State(1) = State(2) -> Ans = State(1) ) or 
    --#               ( State(2) = State(3) -> Ans = State(2) );

end Sensors;


