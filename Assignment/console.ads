
-- Author:              A. Ireland
--
-- Address:             School Mathematical & Computer Sciences
--                      Heriot-Watt University
--                      Edinburgh, EH14 4AS
--
-- E-mail:              a.ireland@hw.ac.uk
--
-- Last modified:       25/9/2013 
--
-- Filename:            reset.adb
--
-- Description:         Models the console associated with the ATP system, i.e. 
--                      the reset mechanism that is required to disable the 
--                      trains's braking system, as well as a SPAD count. 

package Console
  --# own Reset_Status, SPAD_Cnt;
  --# initializes Reset_Status, SPAD_Cnt;
is

   --Delcare our global variables
   Reset_Status: Boolean;
   SPAD_Cnt: Integer;
   
   procedure Enable_Reset;
      --# global out Reset_Status;
      --# derives Reset_Status from ; 
      --# post Reset_Status;

   procedure Disable_Reset;
     --# global out Reset_Status;
     --# derives Reset_Status from ;
     --# post not Reset_Status;

   function Reset_Enabled return Boolean;
     --# global in Reset_Status;  
     --# return Reset_Status;
   
   procedure Inc_SPAD_Cnt;
     --# global in out SPAD_Cnt;
     --# derives SPAD_Cnt from SPAD_Cnt;
     --# pre SPAD_Cnt < Integer'Last;
     --# post SPAD_Cnt = SPAD_Cnt~ + 1;
   
   procedure Reset_SPAD_Cnt;
      --# global out SPAD_Cnt;
      --# derives SPAD_Cnt from ;
      --# post SPAD_Cnt = 0;
   
   function SPAD_Cnt_Value return Integer;
   --# global in SPAD_Cnt;  
   --# return SPAD_Cnt;

end Console;



