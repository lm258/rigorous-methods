
-- Author:              L. Maitland
--
-- Address:             School Mathematical & Computer Sciences
--                      Heriot-Watt University
--                      Edinburgh, EH14 4AS
--
-- E-mail:              lm258@hw.ac.uk
--
-- Filename:            alarm.adb
--
-- Description:         Models the alarm device associated
--                      with the ATP controller.

package body Alarm
is
   -- This will enable the alarm
   procedure Enable
   is
   begin
      State := True;
   end Enable;

   -- This will disable the alarm
   procedure Disable
   is
   begin
      State := False;
   end Disable;

   -- This check if we are enabled or not
   function Enabled return Boolean
   is
   begin
      return State;
   end Enabled;

   -- Init our state to false
   begin
      State := False;

end Alarm;