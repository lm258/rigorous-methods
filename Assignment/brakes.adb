
-- Author:              L. Maitland
--
-- Address:             School Mathematical & Computer Sciences
--                      Heriot-Watt University
--                      Edinburgh, EH14 4AS
--
-- E-mail:              lm258@hw.ac.uk
--
-- Filename:            brakes.adb
-- 
-- Description:         Models the train braking subsystem associated
--                      with the ATP controller.
package body Brakes
is
   -- This procedure will activate the brakes
   procedure Activate
   is
   begin
      State := True;
   end Activate;

   -- This procedure will deactivate the brakes
   procedure Deactivate
   is
   begin
      State := False;
   end Deactivate;

   -- This function will return the state of the brakes
   function Activated return Boolean
   is
   begin
      return State;
   end Activated;

   -- Init our state to false
   begin
      State := False;

end Brakes;